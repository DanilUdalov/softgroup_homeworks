require 'csv'

namespace :user do
  desc 'Import task should have path argument and import users from giving CSV file'
  task import: :environment do
    CSV.foreach('users.csv', headers: true) do |f|
      User.create(first_name: f['first_name'], last_name: f['last_name'],
                  username: f['username'], password: f['password'],
                  email: f['email'], birthday: f['birthday'])
    end
  end

  desc 'Export task should export all Users from database to CSV file'
  task export: :environment do
    CSV.open('users_export.csv', 'wb', headers: true) do |csv|
      csv << User.attribute_names
      User.all.each do |user|
        csv << user.attributes.values
      end
    end
  end
end
