class Post < ActiveRecord::Base
  attr_accessible :title, :body, :published_at, :user_id

  belongs_to :user
  has_many :images, as: :imageable, dependent: :destroy

  scope :post_published, -> { where('published_at IS NOT NULL') }
end
