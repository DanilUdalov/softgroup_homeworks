class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :username,
                  :password, :email, :birthday
  validates :email, uniqueness: true, presence: true
  validates :username, uniqueness: true, presence: true
  validates :password, length: { minimum: 8 }, presence: true

  has_many :posts, dependent: :destroy
  has_many :images, as: :imageable, dependent: :destroy

  scope :adult_users, -> { where('birthday < ?', 18.year.ago) }

  def full_name
    "#{first_name} #{last_name}"
  end
end
