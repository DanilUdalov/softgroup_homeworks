require_relative 'document'
require 'colorize'

url = 'http://www.nokogiri.org/tutorials/installing_nokogiri.html'

puts '1. Enter link;'.colorize(:green)
puts '2. Enter headers h2;'.colorize(:yellow)
puts '0. Exit.'.colorize(:red)

case gets.chomp
when '1' then Link.new(url).parse
when '2' then Header.new(url).parse
when '0' then exit
else p 'Wrong number'
end
