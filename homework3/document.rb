require 'nokogiri'
require 'open-uri'
#
class Document
  def initialize(url)
    @url = url
  end

  def parse
    doc = init_nokogiri
    t = type
    doc.search(t).each { |link| puts link.content }
  end

  private

  def init_nokogiri
    Nokogiri::HTML(open(@url))
  end
end
#
class Link < Document

  private

  def type
    'a'
  end
end
#
class Header < Document

  private

  def type
    'article h2'
  end
end
