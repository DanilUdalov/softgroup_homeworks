require 'mail'
require 'twilio-ruby'
#
module Notification
  def self.included(base)
    base.extend(ClassMethods)
  end

  def add_to_log(recepient)
    open('log.txt', 'a') do |f|
      f.puts("Sending #{self.class} to #{recepient}")
    end
  end

  def send_message(recepient)
    Service::Deliver.new.send(self.class.to_s.downcase, recepient)
  end
  #
  module ClassMethods
    def log
      open('log.txt', 'r') { |f| puts f.read }
    end
  end
end
#
class Email
  include Notification

  def send_message(recepient)
    symb = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
    error = "wrong email format #{recepient}"
    if recepient =~ symb
      add_to_log(recepient)
      puts "Sending #{self.class} to #{recepient}"
    else
      File.open('error.txt', 'a') { |f| f.puts(error) }
      raise ArgumentError, error
    end
  end
end
#
class SMS
  include Notification

  def send_message(recepient)
    symb = /\b[+]?[-0-9\(\)]{10,20}\b/i
    error = "wrong email format #{recepient}"
    if recepient =~ symb
      add_to_log(recepient)
      puts "Sending #{self.class} to #{recepient}"
    else
      File.open('error.txt', 'a') { |f| f.puts(error) }
      raise ArgumentError, error
    end
  end
end
#
module Service
  #
  class Deliver
    def sms(recepient)
      sms_config
      client = Twilio::REST::Client.new

      client.messages.create(
        from: '+15612885176',
        to: recepient,
        body: 'Hello world!'
      )
      puts "Sending SMS to #{recepient}"
    end

    def email(recepient)
      option = option_params
      Mail.defaults do
        delivery_method :smtp, option
      end
      Mail.deliver do
        to recepient
        from 'gtafan51@gmail.com'
        subject 'Hello'
        body 'Hello world'
      end
      puts "Sending email to #{recepient}"
    end

    private

    def option_params
      { address: 'smtp.gmail.com', port: 587,
        domain: 'localhost:3000', user_name: 'gtafan51@gmail.com',
        password: 'mypassword', authentication: 'plain',
        enable_starttls_auto: true }
    end

    def sms_config
      account_sid = 'AC06255986a2b0cfbf5408d758bcd66762'
      auth_token = 'bf8f06348a497e5f269293b42f169c56'

      Twilio.configure do |config|
        config.account_sid = account_sid
        config.auth_token = auth_token
      end
    end
  end
end
