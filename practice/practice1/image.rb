require 'faraday'
#
class Image
  def self.download(url)
    resp = Faraday.get(url)
    case resp.status
    when 400..499 then raise ArgumentError, 'Client error'
    when 500..599 then raise ArgumentError, 'Server error'
    end
    type, exp = resp['content-type'].split('/')
    case type
    when 'image' then open("image.#{exp}", 'wb') { |f| f.write(resp.body) }
    else raise TypeError, 'file is not an image'
    end
  end
end

p Image.download('https://www.hello.com/img_/hello_logo_hero.png')
