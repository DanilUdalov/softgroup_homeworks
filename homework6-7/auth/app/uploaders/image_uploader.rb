# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    [thumb, 'thumb_default.png'].compact.join('_')
  end

  version :square do
    process resize_to_fit: [25, 25]
  end

  version :thumb do
    process resize_to_fit: [250, 150]
  end

  version :medium do
    process resize_to_fit: [600, 400]
  end

  version :large do
    process resize_to_fit: [1000, 800]
  end
end
