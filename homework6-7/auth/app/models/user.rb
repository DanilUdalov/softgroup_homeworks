class User < ActiveRecord::Base
  has_secure_password

  validates_confirmation_of :password
  validates_presence_of :password, on: :create
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :password, length: { minimum: 8 }

  has_many :posts, dependent: :destroy
  has_many :images, as: :imageable, dependent: :destroy
  has_many :accounts, dependent: :destroy

  scope :adult_users, -> { where('birthday < ?', 18.year.ago) }

  def full_name
    "#{ first_name } #{ last_name }"
  end

  def name_space
    if full_name.delete(' ').length.zero?
      return email
    else
      return full_name
    end
  end
end
