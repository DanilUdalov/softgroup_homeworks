class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :user, counter_cache: true
  has_many :images, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :images, allow_destroy: true

  scope :post_published, -> { where('published_at IS NOT NULL') }
end
