class PostsController < ApplicationController
  before_action :post_resourses, only: [:edit, :update, :destroy]

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new(post_params)

    @post.save
    respond_to do |format|
      format.html { redirect_to news_path }
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    @post.update_attributes(post_params)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def destroy
    @post.destroy!
    # respond_to do |format|
    #   format.js { render nothing: true }
    #   format.html { redirect_to news_path }
    # end
  end

  private

  def post_params
    params.require(:post).permit(:title, :body, :published_at, images_attributes: [:id, :url])
  end

  def post_resourses
    @post = Post.find(params[:id])
  end
end
