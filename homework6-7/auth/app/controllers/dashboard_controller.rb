class DashboardController < ApplicationController
  def index
    @post = Post.new
    @post.images.build
    @posts = Post.order('created_at DESC').paginate(page: params[:page], per_page: 20)
  end
end
