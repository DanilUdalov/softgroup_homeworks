class UsersController < ApplicationController
  before_filter :user_resourse, only: [:show, :edit, :update]

  def new
    @user = User.new
  end

  def edit
    redirect_to root_path unless current_user
  end

  def update
    @user.update_attributes(user_params)
    redirect_to action: :show
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to news_path, notice: "Signed up #{ current_user.name_space }"
    else
      render 'new'
    end
  end

  private

  def user_resourse
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :birthday,
                                 :password, :password_confirmation, :username)
  end
end
