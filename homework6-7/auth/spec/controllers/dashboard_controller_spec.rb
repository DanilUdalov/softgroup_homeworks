require 'rails_helper'

RSpec.describe DashboardController, type: :controller do
  let!(:user) { FactoryGirl.create(:user, password: 'qazwsx123', password_confirmation: 'qazwsx123') }
  before { session[:user_id] = user.id }

  subject { response }

  describe '#index' do
    let!(:post) { create(:post) }

    before { get :index }

    it 'assigns @posts' do
      expect(assigns(:posts)).to eq([post])
    end

    it 'assigns @post' do
      expect(assigns(:post)).to be_a_new(Post)
    end
  end
end
