require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  let!(:user) { FactoryGirl.create(:user, password: 'qazwsx123', password_confirmation: 'qazwsx123') }
  before { session[:user_id] = user.id }

  describe '#create' do
    it 'create posts' do
      expect do
        post :create, params: { post: { title: Faker::Lorem.words } }
      end
    end
  end

  describe '#destroy' do
    let!(:post) { FactoryGirl.create(:post, user_id: user.id) }
    before { delete :destroy, id: post.id }

    it { is_expected.to redirect_to news_path }
  end

  describe '#edit' do
    let!(:post) { FactoryGirl.create(:post, user_id: user.id) }

    before { get :edit, id: post.id }
  end
end
