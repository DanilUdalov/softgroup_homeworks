require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  subject { response }

  describe '#new' do
    before { get :new }

    it { is_expected.to render_template(:new) }

    it 'be success' do
      is_expected.to be_success
    end
  end

  describe '#create' do
    let!(:user) { FactoryGirl.attributes_for(:user) }

    context 'valid data' do
      let(:user_params) { FactoryGirl.attributes_for(:user) }

      before { post :create, user: user_params }

      it { expect(session[:user_id]) == User.last.id }

      it { is_expected.to have_http_status(200) }
    end

    context 'invalid data' do
      before { post :create, user: { email: 'invalid', password: 'invalid' } }

      it { is_expected.to render_template('new') }
    end
  end

  describe '#destroy' do
    let!(:user) { FactoryGirl.create(:user, password: 'qazwsx123', password_confirmation: 'qazwsx123') }

    before do
      session[:user_id] = user.id
      delete :destroy, id: user.id
    end

    it { expect(session[:user_id]).to be_nil }

    it { is_expected.to redirect_to root_path }
  end
end
