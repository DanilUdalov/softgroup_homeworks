require 'spec_helper'
require './lib/array.rb'

RSpec.describe Array do
  describe '.top' do
    let(:arr_1) { [3, 2, 4, 5, 4, 3, 3, 3, 1] }

    it 'returns top element' do
      expect(arr_1.top).to eq(3)
    end
  end

  describe '.top(three_elements)' do
    let(:arr_2) { [3, 2, 3, 4, 4, 5, 4, 3, 3, 3, 1, 2] }

    it 'returns top 3 elements' do
      expect(arr_2.top(3)).to eq([3, 4, 2])
    end
  end

  describe '.top(negative_argument)' do
    let(:arr_3) { [3, 2, 3, 4, 4, 5, 4, 3, 3, 3, 1, 2] }

    it 'returns 1 result if argument is negative_argument' do
      expect(arr_3.top(-10)).to eq(3)
    end
  end
end
