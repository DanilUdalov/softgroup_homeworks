require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.create(:user) }

  describe 'associations' do
    it { have_many(:posts) }
    it { have_many(:images) }
    it { have_many(:accounts) }
  end

  describe 'validations' do
    it { have_secure_token }
    it { have_secure_password }
    it { validate_uniqueness_of(:email) }
    it { validate_presence_of(:email) }
    it { validate_presence_of(:password) }
    it { validate_length_of(:password).is_at_least(8) }
    it { validate_confirmation_of(:password) }
  end

  describe '#full_name' do
    subject { FactoryGirl.create(:user, first_name: 'Adam', last_name: 'Sendler', password: 'qazwsx123', password_confirmation: 'qazwsx123') }
    it 'returns fullname' do
      expect(subject.full_name).to eq('Adam Sendler')
    end
  end

  describe '.scopes' do
    let(:all) do
      @user_1 = FactoryGirl.create(:user, birthday: 20.year.ago)
      @user_2 = FactoryGirl.create(:user, birthday: 15.year.ago)
    end

    it 'it older then 18 year' do
      expect(User.adult_users) == [@user_1]
    end
  end
end
