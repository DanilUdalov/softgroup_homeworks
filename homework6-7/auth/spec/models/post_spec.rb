require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:user_1) { FactoryGirl.create(:user) }
  let(:post_1) { FactoryGirl.build(:post, user: user) }

  describe 'associations' do
    it { belong_to(:user) }
    it { have_many(:images) }
  end

  describe 'validations' do
    it { validate_presence_of(:title) }
    it { validate_presence_of(:body) }
  end
end
