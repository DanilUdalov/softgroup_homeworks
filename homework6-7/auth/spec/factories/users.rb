FactoryGirl.define do
  factory :user do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    email Faker::Internet.email
    birthday Faker::Date.birthday(14, 50)
    password Faker::Internet.password(8, 20)
    password_confirmation Faker::Internet.password(10, 20)
    username Faker::Internet.user_name
  end
end
