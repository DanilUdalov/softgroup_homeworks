class AddCounterCacheToUsers < ActiveRecord::Migration
  def up
    add_column :users, :posts_count, :integer, default: 0

    User.reset_column_information
    User.all.each do |user|
      User.update_counters user.id, posts_count: user.posts.length
    end
  end
end
