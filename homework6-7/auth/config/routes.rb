Auth::Application.routes.draw do
  get 'sign_up' => 'users#new', :as => 'sign_up'
  get 'log_in' => 'sessions#new', :as => 'log_in'
  get 'log_out' => 'sessions#destroy', :as => 'log_out'
  get 'news' => 'dashboard#index', :as => 'news'
  get 'auth/:provider/callback', to: 'omniauth#create'

  root to: 'welcome#index'
  resources :users
  resources :sessions
  resources :posts do
    resources :images
  end
end
