require 'csv'
require 'date'
#
class Weather
  def initialize(month)
    @month = month
  end

  def sered_temp
    temp_arr = temp_month
    select_temp = temp_arr.map { |row| row[1].to_i }
    sum_t = select_temp.inject(0) { |sum, x| sum + x }
    sum_t.to_f / temp_arr.size.to_f
  end

  def write_rez
    ser = sered_temp
    puts "Средняя t за месяц #{@month} = #{ser.to_f.round(1)} C"
  end

  private

  def temp_month
    CSV.parse(File.read('file.csv'), headers: true).each.select do |row|
      Date.parse(row['Date']).mon.to_i == @month
    end
  end
end
