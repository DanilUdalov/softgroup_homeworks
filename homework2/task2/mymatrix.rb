#
class MyMatrix
  def initialize(number)
    @number = number
  end

  def matrix
    p arr = create_array
    (arr.size - 1).times do
      p arr.push(arr.shift)
    end
  end

  def valid_number
    return 'You number < 1' if @number <= 1
    Math.sqrt(@number).to_i.downto(2).each do |i|
      return 'addition number' if (@number % i).zero?
    end
    'prime number'
  end

  private

  def create_array
    array = (2..@number)
    array.select { |num| (2..Math.sqrt(num)).none? { |d| (num % d).zero? } }
  end
end
